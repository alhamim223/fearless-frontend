import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App(props) {
  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Conference</th>
          </tr>
        </thead>
        <tbody>
          {/* for (let attendee of props.attendees) {
            <tr>
              <td>{attendee.name }</td>
              <td>{ attendee.conferences }}</td>
            </tr>
          } */
          }
          {props.attendees.map(attendee => {
           return (
            <tr>
              <td>{ attendee.name }</td>
              <td>{attendee.conference}</td>
            </tr>
           )
          })}
        </tbody>
      </table>
    </div>
  );
}


export default App;


